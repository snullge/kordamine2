﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace VeelSpordipäev
{
    class Program
    {
        static void Main(string[] args)
        {
            var protokoll = File.ReadAllLines(@"..\..\spordipäeva protokoll.txt")                               // loeb sisse
                .Skip(1)                                                                                        // jätab esimese rea vahele
                .Select(x => x.Replace(", ", ",").Split(','))                                                   // võtab tühikud ära ja teeb array sõnadest
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })            // loeb sisse nimed, distantsi mis teeb intiks, aja mis teeb intiks
                .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg })              // teeb tegurid Nimi, Distants ja kiirus milleks kasutab algorütmi
                .ToList()                                                                                       // lisab need listi, et pärast saaks kasutada
                ;
            var jooksjad = protokoll.ToLookup(x => x.Nimi);                                                     // jooksijad var on protokolli file nimed

            // kõige kiirema leidmine
            Console.WriteLine("\nKõige kiirem jooksja\n");
            Console.WriteLine(
                protokoll
                .Where(x => x.Distants == 2000)                                                                 //valib distantsiks 2000m
                .OrderByDescending(x => x.Kiirus).FirstOrDefault()?.Nimi ?? "puudub"                            // paneb kiiruse järgi järjekorda, kui pole kedagi siis "puudub"
                );
            // kui on võimalus, et sama kiireid on mitu, siis on veel selline variant
            Console.WriteLine(
                string.Join("\n",

                protokoll
                .GroupBy(x => x.Kiirus).OrderBy(x => x.Key).LastOrDefault()                                    //kui mitu samakiiret siis gruppib kiirused ja paneb tähestiku järjekorras nimed
                )
                );
            // kes on kõige stabiilseim
            Console.WriteLine(
                //string.Join("\n",
                jooksjad
                    .Where(x => x.Count() > 1)                                                                      // jooksja on vähemalt ühe 2 korda jooksnud
                    .Select(x => new { Nimi = x.Key, Min = x.Min(y => y.Kiirus), Max = x.Max(y => y.Kiirus) })      // võtab sisse nime, MIN kiiruse ja MAX kiiruse
                    .Select(x => new { x.Nimi, Vahe = x.Max - x.Min, x.Max, x.Min })                                // võtab nime ja paneb selle järgi VAHE = kiireim kiius - aeglaseim kiirus ja annab max kiiruse ja min kiiruse
                    .OrderBy(x => x.Vahe)
                    .FirstOrDefault()
                //)
                );

        }
    }
}
